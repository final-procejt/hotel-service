package az.hotelservice.mapper;

import az.hotelservice.dto.requests.ReviewRequestDTO;
import az.hotelservice.dto.responses.ReviewResponseDTO;
import az.hotelservice.entity.Hotel;
import az.hotelservice.entity.Review;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = HotelMapper.class)
public interface ReviewMapper {

    @Mapping(source = "hotelId", target = "hotel", qualifiedByName = "mapHotelIdToHotel")
    Review toEntity(ReviewRequestDTO reviewRequestDTO);

    @Mapping(source = "hotel", target = "hotelId", qualifiedByName = "mapHotelIdFromHotel")
    ReviewResponseDTO toDTO(Review review);

    @Mapping(source = "hotelId", target = "hotel.id", qualifiedByName = "mapHotelIdFromReviewRequest")
    void updateReviewFromDto(ReviewRequestDTO reviewRequestDTO, @MappingTarget Review review);

    @Named("mapHotelIdToHotel")
    default Hotel mapHotelIdToHotel(UUID hotelId) {
        if (hotelId == null) {
            return null;
        }
        var hotel = new Hotel();
        hotel.setId(hotelId);
        return hotel;
    }

    @Named("mapHotelIdFromHotel")
    default UUID mapHotelIdFromHotel(Hotel hotel) {
        if (hotel == null) {
            return null;
        }
        return hotel.getId();
    }

    @Named("mapHotelIdFromReviewRequest")
    default UUID mapHotelIdFromReviewRequest(UUID hotelId) {
        return hotelId;
    }
}
