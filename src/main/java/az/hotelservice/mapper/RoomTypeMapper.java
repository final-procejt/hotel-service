package az.hotelservice.mapper;

import az.hotelservice.dto.requests.RoomTypeRequestDTO;
import az.hotelservice.dto.responses.RoomTypeResponseDTO;
import az.hotelservice.entity.RoomType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoomTypeMapper {
    RoomType toEntity(RoomTypeRequestDTO roomTypeRequestDTO);
    RoomTypeResponseDTO toDTO(RoomType roomType);
}