package az.hotelservice.mapper;

import az.hotelservice.dto.AddressDTO;
import az.hotelservice.entity.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    Address toEntity(AddressDTO addressDTO);
    AddressDTO toDTO(Address address);
}
