package az.hotelservice.mapper;

import az.hotelservice.dto.requests.HotelRequestDTO;
import az.hotelservice.dto.responses.HotelResponseDTO;
import az.hotelservice.entity.Hotel;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface HotelMapper {
    Hotel toEntity(HotelRequestDTO hotelRequestDTO);

    HotelResponseDTO toDTO(Hotel hotel);

    void updateHotelFromDto(HotelRequestDTO hotelRequestDTO, @MappingTarget Hotel hotel);
}
