package az.hotelservice.mapper;

import az.hotelservice.dto.requests.RoomRequestDTO;
import az.hotelservice.dto.responses.RoomResponseDTO;
import az.hotelservice.entity.Hotel;
import az.hotelservice.entity.Room;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = {RoomTypeMapper.class, HotelMapper.class})
public interface RoomMapper {

    @Mapping(source = "hotelId", target = "hotel", qualifiedByName = "mapHotelIdToHotel")
    Room toEntity(RoomRequestDTO roomRequestDTO);

    @Mapping(source = "hotel", target = "hotelId", qualifiedByName = "mapHotelIdFromHotel")
    RoomResponseDTO toDTO(Room room);

    @Mapping(source = "hotelId", target = "hotel.id", qualifiedByName = "mapHotelIdFromRoomRequest")
    void updateRoomFromDto(RoomRequestDTO roomRequestDTO, @MappingTarget Room room);

    @Named("mapHotelIdToHotel")
    default Hotel mapHotelIdToHotel(UUID hotelId) {
        if (hotelId == null) {
            return null;
        }
        var hotel = new Hotel();
        hotel.setId(hotelId);
        return hotel;
    }

    @Named("mapHotelIdFromHotel")
    default UUID mapHotelIdFromHotel(Hotel hotel) {
        if (hotel == null) {
            return null;
        }
        return hotel.getId();
    }

    @Named("mapHotelIdFromRoomRequest")
    default UUID mapHotelIdFromRoomRequest(UUID hotelId) {
        return hotelId;
    }
}
