package az.hotelservice.controller;

import az.hotelservice.dto.responses.RestResponse;
import az.hotelservice.dto.responses.RoomResponseDTO;
import az.hotelservice.service.ReservationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.UUID;

@RestController
@RequestMapping("/api/reservation")
@RequiredArgsConstructor
@Slf4j
public class ReservationController {

    private final ReservationService reservationService;

    @PostMapping
    public ResponseEntity<RestResponse<RoomResponseDTO>> reserveRoom(
            @RequestParam UUID hotelId,
            @RequestParam Long roomId,
            @RequestParam LocalDate checkInDate,
            @RequestParam LocalDate checkOutDate) {
        RoomResponseDTO reservedRoom = reservationService.reserveRoom(hotelId, roomId, checkInDate, checkOutDate);
        RestResponse<RoomResponseDTO> response = new RestResponse<>(HttpStatus.OK, reservedRoom);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/reserveCancel")
    public ResponseEntity<RestResponse<Boolean>> cancelReservation(
            @RequestParam UUID hotelId,
            @RequestParam Long roomId) {
        boolean canceled = reservationService.cancelReservation(hotelId, roomId);
        RestResponse<Boolean> response = new RestResponse<>(HttpStatus.OK, canceled);
        return ResponseEntity.ok(response);
    }
}
