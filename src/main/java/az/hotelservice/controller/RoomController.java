package az.hotelservice.controller;

import az.hotelservice.dto.requests.RoomRequestDTO;
import az.hotelservice.dto.responses.RestResponse;
import az.hotelservice.dto.responses.RoomResponseDTO;
import az.hotelservice.service.RoomService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/rooms")
@RequiredArgsConstructor
@Slf4j
public class RoomController {

    private final RoomService roomService;

    @ApiOperation(value = "Create a new room")
    @PostMapping(path = "/create")
    @Operation(summary = "Create a new room")
    public ResponseEntity<RestResponse<RoomResponseDTO>> createRoom(@RequestBody RoomRequestDTO roomRequestDTO) {
        return ResponseEntity.ok(new RestResponse<>(CREATED, roomService.createRoom(roomRequestDTO)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<RestResponse<RoomResponseDTO>> updateRoom(@PathVariable Long id,
                                                                    @RequestBody RoomRequestDTO roomRequestDTO) {
        log.info("Received request to update room with ID: {}", id);
        RoomResponseDTO updatedRoom = roomService.updateRoom(id, roomRequestDTO);
        return ResponseEntity.ok(new RestResponse<>(OK, updatedRoom));
    }

    @GetMapping("/{id}")
    public ResponseEntity<RestResponse<RoomResponseDTO>> getRoomById(@PathVariable Long id) {
        log.info("Received request to get room with ID: {}", id);
        RoomResponseDTO roomResponseDTO = roomService.getRoomById(id);
        return ResponseEntity.ok(new RestResponse<>(OK, roomResponseDTO));
    }

    @GetMapping
    public ResponseEntity<RestResponse<List<RoomResponseDTO>>> getAllRooms() {
        log.info("Received request to get all rooms");
        List<RoomResponseDTO> rooms = roomService.getAllRooms();
        return ResponseEntity.ok(new RestResponse<>(OK, rooms));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RestResponse<Void>> deleteRoom(@PathVariable Long id) {
        log.info("Received request to delete room with ID: {}", id);
        roomService.deleteRoom(id);
        return ResponseEntity.noContent().build();
    }
}
