package az.hotelservice.controller;

import az.hotelservice.dto.requests.HotelRequestDTO;
import az.hotelservice.dto.requests.HotelSearchRequest;
import az.hotelservice.dto.responses.HotelResponseDTO;
import az.hotelservice.dto.responses.PageHotelResponse;
import az.hotelservice.dto.responses.RestResponse;
import az.hotelservice.dto.responses.RoomResponseDTO;
import az.hotelservice.service.HotelService;
import io.swagger.v3.oas.annotations.Operation;
import jdk.jfr.Description;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/hotels")
@RequiredArgsConstructor
@Slf4j
public class HotelController {

    private final HotelService hotelService;

    @PostMapping("/create")
    public ResponseEntity<RestResponse<HotelResponseDTO>> createHotel(@RequestBody HotelRequestDTO hotelRequestDTO) {
        RestResponse<HotelResponseDTO> response =
                new RestResponse<>(CREATED, hotelService.createHotel(hotelRequestDTO));
        return ResponseEntity.ok(response);
    }


    @PutMapping("/update/{id}")
    public ResponseEntity<RestResponse<HotelResponseDTO>> updateHotel(@PathVariable UUID id, @RequestBody HotelRequestDTO hotelRequestDTO) {
        HotelResponseDTO updatedHotel = hotelService.updateHotel(id, hotelRequestDTO);
        RestResponse<HotelResponseDTO> response = new RestResponse<>(OK, updatedHotel);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RestResponse<HotelResponseDTO>> getHotelById(@PathVariable UUID id) {
        HotelResponseDTO hotelResponseDTO = hotelService.getHotelById(id);
        RestResponse<HotelResponseDTO> response = new RestResponse<>(OK, hotelResponseDTO);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = "/allHotels")
    @Operation(summary = "Get all hotel")
    @Description(value = "Enter page and page size")
    public ResponseEntity<RestResponse<PageHotelResponse>> getAllHotel(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        PageHotelResponse pageHotelResponse = hotelService.getAllHotels(page, size);
        RestResponse<PageHotelResponse> response = new RestResponse<>(OK, pageHotelResponse);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteHotel(@PathVariable UUID id) {
        hotelService.deleteHotel(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/search")
    public ResponseEntity<RestResponse<List<HotelResponseDTO>>> searchHotels(
            @RequestParam String country,
            @RequestParam String city,
            @RequestParam Integer roomCapacity,
            @RequestParam BigDecimal minRent,
            @RequestParam BigDecimal maxRent
    ) {
        List<HotelResponseDTO> hotels = hotelService.searchHotels(country, city, roomCapacity, minRent, maxRent);
        return ResponseEntity.ok(new RestResponse<>(OK, hotels));
    }

    @GetMapping(path = "/criteria")
    public ResponseEntity<RestResponse<PageHotelResponse>> searchHotelCriteria(
            @RequestParam(required = false) String country,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) Integer capacity,
            @RequestParam(required = false) BigDecimal minPrice,
            @RequestParam(required = false) BigDecimal maxPrice,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        HotelSearchRequest searchRequest = HotelSearchRequest.builder()
                .country(country)
                .city(city)
                .capacity(capacity)
                .minPrice(minPrice)
                .maxPrice(maxPrice)
                .build();
        PageHotelResponse pageHotelResponse = hotelService.searchHotelCriteria(searchRequest, page, size);
        return ResponseEntity.ok(new RestResponse<>(OK, pageHotelResponse));

    }

    @GetMapping(path = "/{hotelId}/{roomId}")
    public ResponseEntity<RestResponse<HotelResponseDTO>> getRoomByHotelAndRoomId(@PathVariable UUID hotelId,
                                                                                 @PathVariable Long roomId) {
        log.info("Received request to get room with hotel ID: {} and room ID: {}", hotelId, roomId);
        HotelResponseDTO hotelByRoomId = hotelService.getRoomByHotelAndRoomId(hotelId, roomId);
        return ResponseEntity.ok(new RestResponse<>(OK, hotelByRoomId));
    }
}
