package az.hotelservice.controller;

import az.hotelservice.dto.requests.ReviewRequestDTO;
import az.hotelservice.dto.responses.RestResponse;
import az.hotelservice.dto.responses.ReviewResponseDTO;
import az.hotelservice.service.ReviewService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/reviews")
@RequiredArgsConstructor
@Slf4j
public class ReviewController {

    private final ReviewService reviewService;

    @PostMapping
    public ResponseEntity<RestResponse<ReviewResponseDTO>> createReview(@RequestHeader("X-USER-ID") Integer userId,
                                                                        @RequestBody ReviewRequestDTO reviewRequest) {
        return ResponseEntity.ok(new RestResponse<>(HttpStatus.OK, reviewService.createReview(userId, reviewRequest)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<RestResponse<ReviewResponseDTO>> updateReview(@RequestHeader("X-USER-ID") Integer userId,
                                                                        @PathVariable UUID id,
                                                                        @RequestBody ReviewRequestDTO reviewRequest) {
        log.info("Received request to update review with ID: {}", id);
        return ResponseEntity.ok(new RestResponse<>(HttpStatus.OK, reviewService.updateReview(id, userId, reviewRequest)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<RestResponse<ReviewResponseDTO>> getReviewById(@PathVariable UUID id) {
        log.info("Received request to get review with ID: {}", id);
        return ResponseEntity.ok(new RestResponse<>(HttpStatus.OK, reviewService.getReviewById(id)));
    }

    @GetMapping
    public ResponseEntity<RestResponse<List<ReviewResponseDTO>>> getAllReviews() {
        log.info("Received request to get all reviews");
        return ResponseEntity.ok(new RestResponse<>(HttpStatus.OK, reviewService.getAllReviews()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RestResponse<Void>> deleteReview(@PathVariable UUID id) {
        log.info("Received request to delete review with ID: {}", id);
        reviewService.deleteReview(id);
        return ResponseEntity.noContent().build();
    }
}
