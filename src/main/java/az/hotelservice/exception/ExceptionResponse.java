package az.hotelservice.exception;


import java.time.LocalDateTime;

public record ExceptionResponse (

        LocalDateTime timestamp,
        Integer status,
        String message,
        String  error,
        String path
){
}
