package az.hotelservice.exception;

import jakarta.validation.ConstraintViolationException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
                                                                  @NonNull HttpHeaders headers,
                                                                  @NonNull HttpStatusCode status,
                                                                  @NonNull WebRequest request) {
        List<ValidationErrorResponse.Violation> fieldViolations = exception.getBindingResult().getFieldErrors().stream()
                .map(error -> new ValidationErrorResponse.Violation(error.getField(), error.getDefaultMessage()))
                .toList();

        List<ValidationErrorResponse.Violation> globalViolations = exception.getBindingResult().getGlobalErrors().stream()
                .map(error -> new ValidationErrorResponse.Violation(error.getObjectName(), error.getDefaultMessage()))
                .toList();

        List<ValidationErrorResponse.Violation> violations = Stream.concat(fieldViolations.stream(), globalViolations.stream())
                .collect(Collectors.toList());

        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.setViolations(violations);

        log.error("MethodArgumentNotValidException: {}", errorResponse);

        return new ResponseEntity<>(errorResponse, headers, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ValidationErrorResponse> handleConstraintViolationException(ConstraintViolationException exception) {
        List<ValidationErrorResponse.Violation> violations = exception.getConstraintViolations().stream()
                .map(violation -> new ValidationErrorResponse.Violation(
                        violation.getPropertyPath().toString(),
                        violation.getMessage()))
                .collect(Collectors.toList());

        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.setViolations(violations);

        log.error("ConstraintViolationException: {}", errorResponse);

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ExceptionResponse> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception,
                                                                                       WebRequest webRequest) {
        String error = String.format("The parameter '%s' of value '%s' could not be converted to type '%s'",
                exception.getName(), exception.getValue(), Objects.requireNonNull(exception.getRequiredType()).getSimpleName());

        ExceptionResponse errorDetailDTO = new ExceptionResponse(LocalDateTime.now(), HttpStatus.BAD_REQUEST.value(),
                exception.getMessage(), BAD_REQUEST.getReasonPhrase(),
                ((ServletWebRequest) webRequest).getRequest().getRequestURI());

        log.error("MethodArgumentTypeMismatchException: {}", error);

        return new ResponseEntity<>(errorDetailDTO, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<ExceptionResponse> badRequestException(Exception exception, WebRequest request) {
        log.error("BadRequestException: {}", exception.getMessage());
        ExceptionResponse exceptionResponse = new ExceptionResponse(LocalDateTime.now(), BAD_REQUEST.value()
                , exception.getMessage(), BAD_REQUEST.getReasonPhrase(),
                ((ServletWebRequest) request).getRequest().getRequestURI());
        return ResponseEntity.badRequest().body(exceptionResponse);
    }

    @ExceptionHandler(RecordNotFoundException.class)
    public final ResponseEntity<ExceptionResponse> recordNotFound(Exception exception, WebRequest request) {
        log.error("RecordNotFoundException: {}", exception.getMessage());
        ExceptionResponse exceptionResponse = new ExceptionResponse(LocalDateTime.now(), NOT_FOUND.value()
                , exception.getMessage(), NOT_FOUND.getReasonPhrase(),
                ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(exceptionResponse, NOT_FOUND);
    }

    @ExceptionHandler(RoomNotBookedException.class)
    public ResponseEntity<ExceptionResponse> handleRoomNotBookedException(RoomNotBookedException ex, WebRequest request) {
        log.error("RoomNotBookedException: {}", ex.getMessage());
        ExceptionResponse response = new ExceptionResponse(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                "Room Not Booked",
                ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RoomAlreadyBookedException.class)
    public ResponseEntity<ExceptionResponse> handleRoomAlreadyBookedException(RoomAlreadyBookedException ex,
                                                                              WebRequest request) {
        log.error("RoomAlreadyBookedException: {}", ex.getMessage());
        ExceptionResponse response = new ExceptionResponse(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                "Room Already Booked",
                ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ExceptionResponse> exception(Exception exception, WebRequest request) {
        log.error("Exception: {}", exception.getMessage());
        ExceptionResponse exceptionResponse = new ExceptionResponse(LocalDateTime.now(), INTERNAL_SERVER_ERROR.value()
                , exception.getMessage(), INTERNAL_SERVER_ERROR.getReasonPhrase(),
                ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.EXPECTATION_FAILED);
    }
}
