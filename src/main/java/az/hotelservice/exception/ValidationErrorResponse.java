package az.hotelservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ValidationErrorResponse {
    private List<Violation> violations = new ArrayList<>();

    @Override
    public String
    toString() {
        return "ValidationErrorResponse{" +
                "violations=" + violations +
                '}';
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Violation {
        private String fieldName;
        private String message;

        @Override
        public String toString() {
            return "Violation{" +
                    "fieldName='" + fieldName + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    }
}
