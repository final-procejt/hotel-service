package az.hotelservice.exception;

public class RoomNotBookedException extends RuntimeException {
    public RoomNotBookedException(String message) {
        super(message);
    }
}
