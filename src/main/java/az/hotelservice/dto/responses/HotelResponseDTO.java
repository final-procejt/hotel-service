package az.hotelservice.dto.responses;

import az.hotelservice.dto.AddressDTO;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class HotelResponseDTO {

    UUID id;
    String name;
    String hotelPhoto;
    String description;
    Integer stars;
    Double averageRating;
    AddressDTO address;
    List<String> hotelAmenities;
    List<RoomResponseDTO> rooms;
    List<ReviewResponseDTO> reviews;
}
