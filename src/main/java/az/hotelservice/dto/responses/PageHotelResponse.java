package az.hotelservice.dto.responses;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class PageHotelResponse {

    List<HotelResponseDTO> hotelResponse;
    int pageNumber;
    int totalPages;
    long totalElements;
}
