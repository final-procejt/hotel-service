package az.hotelservice.dto.responses;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomResponseDTO {
    String roomPhoto;
    byte[] roomPhoto1;
    Boolean isBooked;
    RoomTypeResponseDTO roomType;
    LocalDate checkInDate;
    LocalDate checkOutDate;
    UUID hotelId;
    double TotalPrice;
}
