package az.hotelservice.dto.responses;

import jakarta.validation.constraints.Min;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class RoomTypeResponseDTO {
    Long id;
    BigDecimal rentPerDay;
    @Min(value = 1)
    Integer roomCapacity;
    Boolean isActive;
}
