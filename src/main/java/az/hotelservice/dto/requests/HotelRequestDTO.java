package az.hotelservice.dto.requests;

import az.hotelservice.dto.AddressDTO;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HotelRequestDTO {

    @NotBlank(message = "Hotel name is mandatory")
    String name;

    String hotelPhoto;

    @NotBlank(message = "Description is mandatory")
    String description;

    @NotNull(message = "Stars are mandatory")
    @Min(value = 1, message = "Stars must be at least 1")
    @Max(value = 5, message = "Stars can be at most 5")
    Integer stars;

    @NotNull(message = "Address is mandatory")
    AddressDTO address;

    List<String> hotelAmenities;
}
