package az.hotelservice.dto.requests;

import lombok.Data;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class RoomTypeRequestDTO {

    @NotNull(message = "Rent per day is mandatory")
    @Min(value = 0, message = "Rent per day must be at least 0")
    BigDecimal rentPerDay;

    @NotNull(message = "Room capacity is mandatory")
    @Min(value = 1, message = "Room capacity must be at least 1")
    Integer roomCapacity;

    @NotNull(message = "Active status is mandatory")
    Boolean isActive;
}
