package az.hotelservice.dto.requests;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class HotelSearchRequest {

    String country;
    String city;
    Integer capacity;
    BigDecimal minPrice;
    BigDecimal maxPrice;

}
