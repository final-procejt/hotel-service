package az.hotelservice.dto.requests;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoomRequestDTO {

    String roomPhoto;
//    List<String> roomPhoto1;
    Boolean isBooked;

    @NotNull(message = "Room type is mandatory")
    RoomTypeRequestDTO roomType;

    @Future(message = "Check-in date must be in the future")
    LocalDate checkInDate;

    @Future(message = "Check-out date must be in the future")
    LocalDate checkOutDate;

    @NotNull(message = "Hotel ID is mandatory")
    UUID hotelId;

}
