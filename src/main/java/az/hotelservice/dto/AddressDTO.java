package az.hotelservice.dto;

import az.hotelservice.validation.MyRule;
import jakarta.persistence.Column;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressDTO {
    String link;
    String street;
    String city;
    String state;
    @MyRule(message = "Pin code must be 5 digits")
    Integer pinCode;
    String country;
    double latitude;
    double longitude;
    public String getFullAddress() {
        return street + ", " + city + ", " + state + ", " + country + " " + pinCode;
    }
}
