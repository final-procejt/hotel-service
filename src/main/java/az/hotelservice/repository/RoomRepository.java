package az.hotelservice.repository;

import az.hotelservice.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RoomRepository extends JpaRepository<Room,Long> {
//    List<Room> findByHotelId(Long hotelId);

    Optional<Room> findByIdAndHotelId(Long roomId, UUID hotelId);

}
