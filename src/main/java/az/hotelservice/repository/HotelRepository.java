package az.hotelservice.repository;

import az.hotelservice.entity.Hotel;
import io.micrometer.common.lang.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface HotelRepository extends JpaRepository<Hotel, UUID>, JpaSpecificationExecutor<Hotel> {

    @NonNull
    @Override
    Page<Hotel> findAll(@NonNull Pageable pageable);

    @Query(value = "SELECT DISTINCT h.* " +
            "FROM hotels h " +
            "JOIN hotel_address a ON h.HOTEL_ADDRESS_ID = a.id " +
            "JOIN Rooms r ON h.id = r.hotel_id " +
            "JOIN room_type rt ON r.room_type_id = rt.id " +
            "WHERE a.country = :country " +
            "AND a.city = :city " +
            "AND rt.room_capacity = :roomCapacity " +
            "AND rt.is_active = TRUE " +
            "AND rt.rent_per_day BETWEEN :minRent AND :maxRent", nativeQuery = true)
    @NonNull
    List<Hotel> findHotelsByCriteria(
            @Param("country") String country,
            @Param("city") String city,
            @Param("roomCapacity") int roomCapacity,
            @Param("minRent") BigDecimal minRent,
            @Param("maxRent") BigDecimal maxRent);

}
