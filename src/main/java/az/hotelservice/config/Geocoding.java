package az.hotelservice.config;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;

public class Geocoding {

//    @Value("${google.api.key}")
//    private static String apiKey;

    private static final String API_KEY = "AIzaSyBUHRRC5nqNtMpcdm1p_9PGU0_9fuEA40I";

    public static Location getCoordinates(String address) throws Exception {
        String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" +
                address.replace(" ", "%20") + "&key=" + API_KEY;
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setRequestMethod("GET");
        InputStream inputStream = conn.getInputStream();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(inputStream);
        JsonNode resultsNode = rootNode.path("results");
        if (resultsNode.isEmpty()) {
            System.out.println("Full JSON response: " + rootNode.toPrettyString());
            throw new RuntimeException("Geocoding API returned no results for the address: " + address);
        }
        JsonNode locationNode = resultsNode.get(0).path("geometry").path("location");
        if (locationNode.isMissingNode()) {
            System.out.println("Full JSON response: " + rootNode.toPrettyString());
            throw new RuntimeException("Geocoding API response is missing location data for the address: " + address);
        }
        double latitude = locationNode.path("lat").asDouble();
        double longitude = locationNode.path("lng").asDouble();

        return new Location(latitude, longitude);
    }

    public static class Location {
        private double latitude;
        private double longitude;

        public Location(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }
}
