package az.hotelservice.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@OpenAPIDefinition
public class SpringDocumentConfig implements WebMvcConfigurer {

    @Bean
    public OpenAPI openApi(){
        return new OpenAPI().info(
                new Info()
                        .title("Hotel Configuration")
                        .version("0.0.1")
                        .description("Service for hotel!")
                        .contact(
                                new Contact()
                                        .url("www.google.com")
                                        .email("xalil.dg@gmail.com")
                                        .name("Khalil Yusifov")
                        )
        );
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
