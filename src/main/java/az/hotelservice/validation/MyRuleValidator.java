package az.hotelservice.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class MyRuleValidator implements ConstraintValidator<MyRule,String> {


    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value==null){
            return false;
        }
        return value.matches("[0-9]{5}");
    }
}
