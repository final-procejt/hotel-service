package az.hotelservice.service.impl;

import az.hotelservice.dto.requests.RoomRequestDTO;
import az.hotelservice.dto.responses.RoomResponseDTO;
import az.hotelservice.entity.Room;
import az.hotelservice.exception.RecordNotFoundException;
import az.hotelservice.mapper.RoomMapper;
import az.hotelservice.repository.RoomRepository;
import az.hotelservice.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;
    private final RoomMapper roomMapper;
    private final FileStorageService fileStorageService;

    @Override
    public RoomResponseDTO createRoom(RoomRequestDTO roomRequestDTO) {
        log.info("Creating a new room with details: {}", roomRequestDTO);
        Room room = roomMapper.toEntity(roomRequestDTO);
        Room savedRoom = roomRepository.save(room);
        log.info("Room created successfully with ID: {}", savedRoom.getId());
        return roomMapper.toDTO(savedRoom);
    }


    @Override
    public RoomResponseDTO updateRoom(Long id, RoomRequestDTO roomRequestDTO) {
        log.info("Updating room with ID: {}", id);
        return roomRepository.findById(id).map(existingRoom -> {
            roomMapper.updateRoomFromDto(roomRequestDTO, existingRoom);
            Room updatedRoom = roomRepository.save(existingRoom);
            log.info("Room updated successfully with ID: {}", updatedRoom.getId());
            return roomMapper.toDTO(updatedRoom);
        }).orElseThrow(() -> new RecordNotFoundException("Room not found with ID: "+ id));
    }

    @Override
    @Transactional(readOnly = true)
    public RoomResponseDTO getRoomById(Long id) {
        log.info("Fetching room with ID: {}", id);
        Room room = roomRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Room not found with ID: " + id));
        return roomMapper.toDTO(room);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RoomResponseDTO> getAllRooms() {
        log.info("Fetching all rooms");
        List<Room> rooms = roomRepository.findAll();
        return rooms.stream().map(roomMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public void deleteRoom(Long id) {
        log.info("Deleting room with ID: {}", id);
        roomRepository.deleteById(id);
        log.info("Room deleted successfully with ID: {}", id);
    }
}
