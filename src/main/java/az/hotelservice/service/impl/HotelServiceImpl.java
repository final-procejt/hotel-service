package az.hotelservice.service.impl;

import az.hotelservice.config.Geocoding;
import az.hotelservice.criteria.HotelSpecifications;
import az.hotelservice.dto.requests.HotelRequestDTO;
import az.hotelservice.dto.requests.HotelSearchRequest;
import az.hotelservice.dto.responses.HotelResponseDTO;
import az.hotelservice.dto.responses.PageHotelResponse;
import az.hotelservice.dto.responses.RoomResponseDTO;
import az.hotelservice.entity.Hotel;
import az.hotelservice.entity.Review;
import az.hotelservice.entity.Room;
import az.hotelservice.exception.RecordNotFoundException;
import az.hotelservice.mapper.HotelMapper;
import az.hotelservice.mapper.RoomMapper;
import az.hotelservice.repository.HotelRepository;
import az.hotelservice.repository.RoomRepository;
import az.hotelservice.service.HotelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class HotelServiceImpl implements HotelService {

    private final HotelRepository hotelRepository;
    private final RoomRepository roomRepository;
    private final HotelMapper hotelMapper;
    private final RoomMapper roomMapper;

    @Transactional
    @Override
    public HotelResponseDTO createHotel(HotelRequestDTO hotelRequestDTO) {
        log.info("Creating a new hotel with details: {}", hotelRequestDTO);
        Hotel hotel = hotelMapper.toEntity(hotelRequestDTO);
        try {
            Geocoding.Location location = Geocoding.getCoordinates(hotelRequestDTO.getAddress().getFullAddress());
            log.info("Location {}", location);
            hotel.getAddress().setLatitude(location.getLatitude());
            hotel.getAddress().setLongitude(location.getLongitude());
            log.info("Locationdd {}", hotel.getAddress().getLatitude());
        } catch (Exception e) {
            log.error("Error occurred while getting coordinates for the address: {}", hotelRequestDTO.getAddress(), e);
            throw new RuntimeException("Failed to get coordinates for the address: " +
                    hotelRequestDTO.getAddress().getFullAddress());
        }
        Hotel savedHotel = hotelRepository.save(hotel);
        log.info("Hotel created successfully with ID: {}", savedHotel.getId());
        return hotelMapper.toDTO(savedHotel);
    }


    @Transactional
    @Override
    public HotelResponseDTO updateHotel(UUID id, HotelRequestDTO hotelRequestDTO) {
        log.info("Updating hotel with ID: {}", id);
        return hotelRepository.findById(id).map(existingHotel -> {
            hotelMapper.updateHotelFromDto(hotelRequestDTO, existingHotel);
            Hotel updatedHotel = hotelRepository.save(existingHotel);
            log.info("Hotel updated successfully with ID: {}", updatedHotel.getId());
            return hotelMapper.toDTO(updatedHotel);
        }).orElseThrow(() -> new RecordNotFoundException("Hotel not found with ID: " + id));
    }

    @Override
    public HotelResponseDTO getHotelById(UUID id) {
        log.info("Fetching hotel with ID: {}", id);
        Hotel hotel = hotelRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Hotel not found with ID: " + id));
        return hotelMapper.toDTO(hotel);
    }


    @Transactional(readOnly = true)
    @Override
    public PageHotelResponse getAllHotels(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Hotel> allHotels = hotelRepository.findAll(pageable);
        if (allHotels.isEmpty()) {
            throw new RecordNotFoundException("No hotels found for the given page and size");
        }
        List<HotelResponseDTO> list = allHotels.getContent().stream().map(hotelMapper::toDTO).toList();
        log.info("Fetching all hotels {}", allHotels.getTotalElements());
        return buildPageResponse(allHotels, list);
    }

    private PageHotelResponse buildPageResponse(Page<Hotel> hotelPages, List<HotelResponseDTO> responseDTOS) {
        return PageHotelResponse.builder()
                .hotelResponse(responseDTOS)
                .pageNumber(hotelPages.getNumber() + 1)
                .totalPages(hotelPages.getTotalPages())
                .totalElements(hotelPages.getTotalElements())
                .build();
    }

    @Override
    public PageHotelResponse searchHotelCriteria(HotelSearchRequest searchRequest, int page, int size) {
        Specification<Hotel> hotelSpecification = HotelSpecifications.searchByCriteria(searchRequest);
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<Hotel> allHotels = hotelRepository.findAll(hotelSpecification, pageRequest);
        if (allHotels.isEmpty()) {
            throw new RecordNotFoundException("No hotels found for the given page and size");
        }
        List<HotelResponseDTO> hotelResponseDTOList = allHotels.getContent().stream().map(hotelMapper::toDTO).toList();
        return PageHotelResponse.builder()
                .hotelResponse(hotelResponseDTOList)
                .pageNumber(allHotels.getNumber() + 1)
                .totalPages(allHotels.getTotalPages())
                .totalElements(allHotels.getTotalElements())
                .build();
    }


    @Override
    public void deleteHotel(UUID id) {
        log.info("Deleting hotel with ID: {}", id);
        hotelRepository.deleteById(id);
        log.info("Hotel deleted successfully with ID: {}", id);
    }

    @Transactional(readOnly = true)
    public List<HotelResponseDTO> searchHotels(String country, String city, Integer roomCapacity, BigDecimal minRent,
                                               BigDecimal maxRent) {
        List<Hotel> hotelsByCriteria = hotelRepository.findHotelsByCriteria(country, city, roomCapacity, minRent,
                maxRent);
        return hotelsByCriteria.stream().map(hotelMapper::toDTO).toList();
    }

    public void updateHotelOverallRating(UUID id) {
        Hotel hotel = hotelRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Hotel not found"));
        List<Review> reviews = hotel.getReviews();
        Double averageRating = reviews.stream()
                .mapToInt(Review::getRating)
                .average()
                .orElse(0.0);

        hotel.setAverageRating(averageRating);
        hotelRepository.save(hotel);
    }

    @Override
    public HotelResponseDTO getRoomByHotelAndRoomId(UUID hotelId, Long roomId) {
        Hotel hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new RecordNotFoundException("Hotel not found with the provided hotelId."));
        Room room = roomRepository.findByIdAndHotelId(roomId, hotelId)
                .orElseThrow(() -> new RecordNotFoundException("Room not found with the provided hotelId and roomId."));
        HotelResponseDTO hotelResponse = hotelMapper.toDTO(hotel);
        RoomResponseDTO responseRoom = roomMapper.toDTO(room);
        hotelResponse.setRooms(List.of(responseRoom));
        return hotelResponse;
    }
}
