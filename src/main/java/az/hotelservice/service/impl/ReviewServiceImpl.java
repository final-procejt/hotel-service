package az.hotelservice.service.impl;

import az.hotelservice.dto.requests.ReviewRequestDTO;
import az.hotelservice.dto.responses.ReviewResponseDTO;
import az.hotelservice.entity.Review;
import az.hotelservice.exception.RecordNotFoundException;
import az.hotelservice.mapper.ReviewMapper;
import az.hotelservice.repository.ReviewRepository;
import az.hotelservice.service.HotelService;
import az.hotelservice.service.ReviewService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;
    private final HotelService hotelService;
    private final ReviewMapper reviewMapper;

    @Override
    public ReviewResponseDTO createReview(Integer userId, ReviewRequestDTO reviewRequest) {
        log.info("Creating a new review with details: {}", reviewRequest);
        Review review = reviewMapper.toEntity(reviewRequest);
        review.setUserId(userId);
        Review savedReview = reviewRepository.save(review);

        hotelService.updateHotelOverallRating(reviewRequest.getHotelId());

        log.info("Review created successfully with ID: {}", savedReview.getId());
        return reviewMapper.toDTO(savedReview);
    }

    @Override
    public ReviewResponseDTO updateReview(UUID reviewId, Integer userId, ReviewRequestDTO reviewRequest) {
        log.info("Updating review with ID: {}", reviewId);
        Review review = reviewRepository.findById(reviewId)
                .orElseThrow(() -> new RecordNotFoundException("Review not found with ID: " + reviewId));
        review.setUserId(userId);
        reviewMapper.updateReviewFromDto(reviewRequest, review);
        Review updatedReview = reviewRepository.save(review);

        log.info("Review updated successfully with ID: {}", updatedReview.getId());
        return reviewMapper.toDTO(updatedReview);
    }

    @Override
    @Transactional(readOnly = true)
    public ReviewResponseDTO getReviewById(UUID id) {
        log.info("Fetching review with ID: {}", id);
        Review review = reviewRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Review not found with ID: " + id));
        return reviewMapper.toDTO(review);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ReviewResponseDTO> getAllReviews() {
        log.info("Fetching all reviews");
        List<Review> reviews = reviewRepository.findAll();
        return reviews.stream().map(reviewMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public void deleteReview(UUID id) {
        log.info("Deleting review with ID: {}", id);
        reviewRepository.deleteById(id);
        log.info("Review deleted successfully with ID: {}", id);
    }
}
