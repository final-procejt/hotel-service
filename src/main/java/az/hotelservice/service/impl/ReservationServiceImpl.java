package az.hotelservice.service.impl;

import az.hotelservice.dto.responses.RoomResponseDTO;
import az.hotelservice.entity.Room;
import az.hotelservice.entity.RoomType;
import az.hotelservice.exception.RecordNotFoundException;
import az.hotelservice.exception.RoomAlreadyBookedException;
import az.hotelservice.exception.RoomNotBookedException;
import az.hotelservice.mapper.RoomMapper;
import az.hotelservice.repository.RoomRepository;
import az.hotelservice.repository.RoomTypeRepository;
import az.hotelservice.service.ReservationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReservationServiceImpl implements ReservationService {

    private final RoomRepository roomRepository;
    private final RoomTypeRepository roomTypeRepository;
    private final RoomMapper roomMapper;

    @Transactional
    @Override
    public RoomResponseDTO reserveRoom(UUID hotelId, Long roomId, LocalDate checkInDate, LocalDate checkOutDate) {
        if (checkInDate.isAfter(checkOutDate)) {
            throw new RoomAlreadyBookedException("Check-in date cannot be after check-out date");
        }
        Long daysBetween = ChronoUnit.DAYS.between(checkInDate, checkOutDate);
        Room room = roomRepository.findByIdAndHotelId(roomId, hotelId)
                .orElseThrow(() -> new RecordNotFoundException("Room not found"));
        if (room.getCheckInDate() != null && room.getCheckOutDate() != null) {
            if ((checkInDate.isBefore(room.getCheckOutDate()) && checkOutDate.isAfter(room.getCheckInDate())) ||
                    (checkInDate.isEqual(room.getCheckInDate()) && checkOutDate.isEqual(room.getCheckOutDate()))) {
                throw new RoomAlreadyBookedException("Room is already booked for the given dates");
            }
        }
        room.setCheckInDate(checkInDate);
        room.setCheckOutDate(checkOutDate);
        RoomType roomType = room.getRoomType();
        roomType.setIsActive(false);
        roomTypeRepository.save(roomType);
        Room savedRoom = roomRepository.save(room);
        RoomResponseDTO responseDto = roomMapper.toDTO(savedRoom);
        double totalPrice = daysBetween * roomType.getRentPerDay();
        responseDto.setTotalPrice(totalPrice);
        log.info("Room reserved successfully with ID and total price: {} {}", savedRoom.getId(), totalPrice);
        return responseDto;
    }


    @Transactional
    @Override
    public Boolean cancelReservation(UUID hotelId, Long roomId) {
        Room room = roomRepository.findByIdAndHotelId(roomId, hotelId)
                .orElseThrow(() -> new RecordNotFoundException("Room not found"));
        if (room.getCheckInDate() == null || room.getCheckOutDate() == null) {
            throw new RoomNotBookedException("Room is not booked");
        }
        room.setCheckInDate(null);
        room.setCheckOutDate(null);
        RoomType roomType = room.getRoomType();
        roomType.setIsActive(true);
        roomTypeRepository.save(roomType);
        roomRepository.save(room);
        return true;
    }
}
