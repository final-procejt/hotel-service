package az.hotelservice.service;

import az.hotelservice.dto.responses.RoomResponseDTO;

import java.time.LocalDate;
import java.util.UUID;

public interface ReservationService {

    RoomResponseDTO reserveRoom(UUID hotelId, Long roomId, LocalDate checkInDate, LocalDate checkOutDate);
    Boolean cancelReservation(UUID hotelId, Long roomId);
}
