package az.hotelservice.service;

import az.hotelservice.dto.requests.RoomRequestDTO;
import az.hotelservice.dto.responses.RoomResponseDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface RoomService {
    RoomResponseDTO createRoom(RoomRequestDTO roomRequestDTO);
    RoomResponseDTO updateRoom(Long id, RoomRequestDTO roomRequestDTO);
    RoomResponseDTO getRoomById(Long id);
    List<RoomResponseDTO> getAllRooms();
    void deleteRoom(Long id);
}