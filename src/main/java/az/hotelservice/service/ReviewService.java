package az.hotelservice.service;

import az.hotelservice.dto.requests.ReviewRequestDTO;
import az.hotelservice.dto.responses.ReviewResponseDTO;

import java.util.List;
import java.util.UUID;

public interface ReviewService {
    ReviewResponseDTO createReview(Integer userId, ReviewRequestDTO reviewRequestDTO);
    ReviewResponseDTO updateReview(UUID id, Integer userId, ReviewRequestDTO reviewRequestDTO);
    ReviewResponseDTO getReviewById(UUID id);
    List<ReviewResponseDTO> getAllReviews();
    void deleteReview(UUID id);
}
