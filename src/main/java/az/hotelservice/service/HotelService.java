package az.hotelservice.service;

import az.hotelservice.dto.requests.HotelRequestDTO;
import az.hotelservice.dto.requests.HotelSearchRequest;
import az.hotelservice.dto.responses.HotelResponseDTO;
import az.hotelservice.dto.responses.PageHotelResponse;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface HotelService {
    HotelResponseDTO createHotel(HotelRequestDTO hotelRequestDTO);

    HotelResponseDTO updateHotel(UUID hotelId, HotelRequestDTO hotelRequestDTO);

    HotelResponseDTO getHotelById(UUID hotelId);

    PageHotelResponse getAllHotels(int page, int size);

    void deleteHotel(UUID hotelId);

    PageHotelResponse searchHotelCriteria(HotelSearchRequest searchRequest, int page, int size);

    List<HotelResponseDTO> searchHotels(String country, String city, Integer roomCapacity,
                                        BigDecimal minRent, BigDecimal maxRent);
    void updateHotelOverallRating(UUID hotelId);

    HotelResponseDTO getRoomByHotelAndRoomId(UUID hotelId, Long roomId);

}
