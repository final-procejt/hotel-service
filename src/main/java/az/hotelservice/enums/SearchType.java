package az.hotelservice.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SearchType {

    COUNTRY(""),
    CITY(""),
    MIN_CAPACITY("^[1-9]$|^1[0-5]$"),
    MAX_CAPACITY("^[2-9]$|^1[0-5]$"),
    MIN_PRICE("^[1-9]$|^[1-9][0-9]{1,3}$"),
    MAX_PRICE("^[1-9][0-9]$|^[1-9][0-9]{2,3}$"),
    STARS("^[1-5]$"),
    ROOM_CAPACITY("^[1-9]$|^1[0-5]$");

    private final String regex;
}
