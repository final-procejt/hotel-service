package az.hotelservice.criteria;

import az.hotelservice.dto.requests.HotelSearchRequest;
import az.hotelservice.entity.Address;
import az.hotelservice.entity.Hotel;
import az.hotelservice.entity.Room;
import az.hotelservice.entity.RoomType;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HotelSpecifications {

    public static Specification<Hotel> searchByCriteria(HotelSearchRequest searchRequest){
        return (root,query,criteriaBuilder) -> {
            List<Predicate> predicates=new ArrayList<>();
            Join<Hotel, Address> addressJoin=root.join("address");
            Join<Hotel, Room> roomJoin=root.join("rooms");
            Join<Room, RoomType> roomTypeJoin=roomJoin.join("roomType");
            if (StringUtils.isNotBlank(searchRequest.getCity())){
//                predicates.add(criteriaBuilder.equal(addressJoin.get("city"), searchRequest.getCity()));
                Predicate city=criteriaBuilder.like(addressJoin.get("city"),
                        "%" + searchRequest.getCity() + "%");
                predicates.add(city);
            }
            if (StringUtils.isNotBlank(searchRequest.getCountry())){
//                predicates.add(criteriaBuilder.equal(addressJoin.get("country"),searchRequest.getCountry()));
                Predicate country=criteriaBuilder.like(addressJoin.get("country"),
                        "%" + searchRequest.getCountry() + "%");
                predicates.add(country);
            }
             if (searchRequest.getCapacity() != null && searchRequest.getCapacity()>0){
                 predicates.add(criteriaBuilder.equal(roomTypeJoin.get("roomCapacity"), searchRequest.getCapacity()));
            }
             if (searchRequest.getMinPrice() != null){
                 predicates.add(criteriaBuilder.greaterThanOrEqualTo(roomTypeJoin.get("rentPerDay"),
                         searchRequest.getMinPrice()));

            } if (searchRequest.getMaxPrice() != null){
                predicates.add(criteriaBuilder.lessThanOrEqualTo(roomTypeJoin.get("rentPerDay"),
                        searchRequest.getMaxPrice()));
            }
            predicates.add(criteriaBuilder.isTrue(roomTypeJoin.get("isActive")));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
