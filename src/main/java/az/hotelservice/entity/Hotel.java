package az.hotelservice.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "hotels")
@NamedEntityGraph(name = "Hotel.rooms",
        attributeNodes = @NamedAttributeNode("rooms"))
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@SuperBuilder
public class Hotel extends BaseEntity {

    @NotNull
    @NotEmpty(message = "Hotel name must not be empty")
    @Size(min = 2, message = "Hotel name must be at least 2 characters", max = 30)
    String name;
    String hotelPhoto;
    String description;
    @Min(value = 1, message = "Stars must be at least 1")
    @Max(value = 5, message = "Stars must be at most 5")
    Integer stars;

    Double averageRating;

    @OneToMany(mappedBy = "hotel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<Room> rooms = new ArrayList<>();

    @OneToMany(mappedBy = "hotel", cascade = CascadeType.ALL)
    List<Review> reviews = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "HOTEL_ADDRESS_ID")
    Address address;

    @ElementCollection
    @CollectionTable(name = "hotel_amenities", joinColumns = @JoinColumn(name = "hotel_id"))
    @Column(name = "amenity")
    List<String> hotelAmenities = new ArrayList<>();

    @Override
    public String toString() {
        return "Hotel{" +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", stars=" + stars +
                ", hotelAddress=" + (address != null ? address.getId() : null) +
                ", rooms=" + (rooms != null ? rooms.size() : 0) +
                ", hotelAmenities=" + hotelAmenities +
                '}';
    }

}
