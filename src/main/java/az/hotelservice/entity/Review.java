package az.hotelservice.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "reviews")
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class Review extends BaseEntity {

    Integer rating;
    String message;
    Integer userId;

    @ManyToOne
    @JoinColumn(name = "hotel_id", nullable = false)
    Hotel hotel;

    @Override
    public String toString() {
        return "Review{" +
                "rating=" + rating +
                ", message='" + message + '\'' +
                ", userId=" + userId +
                ", hotel=" + hotel +
                '}';
    }
}
