package az.hotelservice.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.Future;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Entity(name = "rooms")
@DiscriminatorValue("rooms")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String roomPhoto;

//    @Lob
//    @Column(name = "room_photo1", columnDefinition = "bytea")
//    byte[] roomPhoto1;

    LocalDate checkInDate;
    @Future
    LocalDate checkOutDate;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "room_type_id")
    RoomType roomType;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    @JsonBackReference
    Hotel hotel;

    @Override
    public String toString() {
        return "Room{" +
                "photo='" + roomPhoto + '\'' +
//                "photo1='" + roomPhoto1 + '\'' +
                ", RoomType='" + roomType + '\'' +
                ", hotel=" + hotel +
                '}';
    }

}
