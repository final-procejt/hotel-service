package az.hotelservice.service.impl;

import az.hotelservice.dto.requests.ReviewRequestDTO;
import az.hotelservice.dto.responses.ReviewResponseDTO;
import az.hotelservice.entity.Review;
import az.hotelservice.exception.RecordNotFoundException;
import az.hotelservice.mapper.ReviewMapper;
import az.hotelservice.repository.ReviewRepository;
import az.hotelservice.service.HotelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ReviewServiceImplTest {
    @Mock
    private ReviewRepository reviewRepository;

    @Mock
    private ReviewMapper reviewMapper;
    @Mock
    private HotelService hotelService;

    @InjectMocks
    private ReviewServiceImpl reviewService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateReview() {
        ReviewRequestDTO requestDTO = new ReviewRequestDTO();
        UUID hotelId = UUID.randomUUID();
        requestDTO.setHotelId(hotelId);
        Review review = new Review();
        Review savedReview = new Review();
        ReviewResponseDTO responseDTO = new ReviewResponseDTO();
        when(reviewMapper.toEntity(requestDTO)).thenReturn(review);
        when(reviewRepository.save(review)).thenReturn(savedReview);
        when(reviewMapper.toDTO(savedReview)).thenReturn(responseDTO);
        doNothing().when(hotelService).updateHotelOverallRating(hotelId);
        ReviewResponseDTO result = reviewService.createReview(1, requestDTO);
        assertNotNull(result);
        assertEquals(responseDTO, result);
        verify(reviewMapper, times(1)).toEntity(requestDTO);
        verify(reviewRepository, times(1)).save(review);
        verify(reviewMapper, times(1)).toDTO(savedReview);
        verify(hotelService, times(1)).updateHotelOverallRating(hotelId);
    }

    @Test
    public void testUpdateReview() {
        UUID reviewId = UUID.randomUUID();
        ReviewRequestDTO requestDTO = new ReviewRequestDTO();
        Review review = new Review();
        review.setId(reviewId);
        Review updatedReview = new Review();
        ReviewResponseDTO responseDTO = new ReviewResponseDTO();
        when(reviewRepository.findById(reviewId)).thenReturn(Optional.of(review));
        when(reviewRepository.save(review)).thenReturn(updatedReview);
        when(reviewMapper.toDTO(updatedReview)).thenReturn(responseDTO);
        ReviewResponseDTO result = reviewService.updateReview(reviewId, 1, requestDTO);
        assertNotNull(result);
        assertEquals(result, responseDTO);
        verify(reviewRepository, times(1)).findById(reviewId);
        verify(reviewRepository, times(1)).save(review);
        verify(reviewMapper, times(1)).toDTO(updatedReview);
    }

    @Test
    public void testGetReviewById() {
        UUID id = UUID.randomUUID();
        Review review = new Review();
        ReviewResponseDTO responseDTO = new ReviewResponseDTO();
        when(reviewRepository.findById(id)).thenReturn(Optional.of(review));
        when(reviewMapper.toDTO(review)).thenReturn(responseDTO);
        ReviewResponseDTO result = reviewService.getReviewById(id);
        assertNotNull(result);
        assertEquals(result, responseDTO);
        verify(reviewRepository, times(1)).findById(id);
        verify(reviewMapper, times(1)).toDTO(review);
    }

    @Test
    public void testDeleteReview() {
        UUID id = UUID.randomUUID();
        doNothing().when(reviewRepository).deleteById(id);
        reviewService.deleteReview(id);
        verify(reviewRepository, times(1)).deleteById(id);
    }

    @Test
    public void testGetAllReviews() {
        ReviewResponseDTO responseDTO = new ReviewResponseDTO();
        List<Review> reviewList = new ArrayList<>();
        when(reviewRepository.findAll()).thenReturn(reviewList);
        when(reviewMapper.toDTO(any(Review.class))).thenReturn(responseDTO);
        List<ReviewResponseDTO> responseDTOS = reviewService.getAllReviews();
        assertNotNull(responseDTOS);
        verify(reviewRepository, times(1)).findAll();
    }

    @Test
    public void testReviewNotUpdated() {
        UUID reviewId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        ReviewRequestDTO requestDTO = new ReviewRequestDTO();
        when(reviewRepository.findById(reviewId)).thenReturn(Optional.empty());
        assertThrows(RecordNotFoundException.class, () -> reviewService.updateReview(userId, 1, requestDTO));
    }

    @Test
    public void testReviewByIdNotFound() {
        UUID id = UUID.randomUUID();
        when(reviewRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(RecordNotFoundException.class, () -> reviewService.getReviewById(id));
    }
}
