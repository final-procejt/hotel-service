package az.hotelservice.service.impl;

import az.hotelservice.dto.requests.RoomRequestDTO;
import az.hotelservice.dto.responses.RoomResponseDTO;
import az.hotelservice.entity.Room;
import az.hotelservice.exception.RecordNotFoundException;
import az.hotelservice.mapper.RoomMapper;
import az.hotelservice.repository.RoomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class RoomServiceImplTest {
    @Mock
    private RoomRepository roomRepository;

    @Mock
    private RoomMapper roomMapper;

    @InjectMocks
    private RoomServiceImpl roomService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

//    @Test
//    public void testCreateRoom() {
//        RoomRequestDTO requestDTO = new RoomRequestDTO();
//        Room room = new Room();
//        Room savedRoom = new Room();
//        RoomResponseDTO responseDTO = new RoomResponseDTO();
//
//        when(roomMapper.toEntity(requestDTO)).thenReturn(room);
//        when(roomRepository.save(room)).thenReturn(savedRoom);
//        when(roomMapper.toDTO(savedRoom)).thenReturn(responseDTO);
//
//        RoomResponseDTO result = roomService.createRoom(requestDTO);
//
//        assertNotNull(result);
//        assertEquals(responseDTO, result);
//
//        verify(roomMapper, times(1)).toEntity(requestDTO);
//        verify(roomRepository, times(1)).save(room);
//        verify(roomMapper, times(1)).toDTO(savedRoom);
//    }

    @Test
    public void testUpdateRoom() {
        Long id = 1L;
        RoomRequestDTO requestDTO = new RoomRequestDTO();
        Room room = new Room();
        room.setId(id);
        Room updatedRoom = new Room();
        RoomResponseDTO roomResponseDTO = new RoomResponseDTO();

        when(roomRepository.findById(id)).thenReturn(Optional.of(room));
        when(roomRepository.save(room)).thenReturn(updatedRoom);
        when(roomMapper.toDTO(updatedRoom)).thenReturn(roomResponseDTO);

        RoomResponseDTO result = roomService.updateRoom(id, requestDTO);

        assertNotNull(result);
        assertEquals(roomResponseDTO, result);

        verify(roomRepository, times(1)).findById(id);
        verify(roomRepository, times(1)).save(room);
        verify(roomMapper, times(1)).toDTO(updatedRoom);
    }

    @Test
    public void testNotUpdateRoom() {
        Long id = 1L;
        RoomRequestDTO roomRequestDTO = new RoomRequestDTO();

        when(roomRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, ()->roomService.updateRoom(id, roomRequestDTO));
    }

    @Test
    public void testGetRoomById() {
        Long id = 1L;
        RoomResponseDTO responseDTO = new RoomResponseDTO();
        Room room = new Room();
        room.setId(id);

        when(roomRepository.findById(id)).thenReturn(Optional.of(room));
        when(roomMapper.toDTO(room)).thenReturn(responseDTO);

        RoomResponseDTO result = roomService.getRoomById(id);

        assertNotNull(result);
        assertEquals(responseDTO, result);

        verify(roomRepository, times(1)).findById(id);
        verify(roomMapper, times(1)).toDTO(room);
    }

    @Test
    public void testGetRoomByIdNotFound() {
        Long id = 1L;

        when(roomRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, () -> roomService.getRoomById(id));
    }

    @Test
    public void testDeleteRoom() {
        Long id = 1L;
        roomService.deleteRoom(id);
        verify(roomRepository, times(1)).deleteById(id);
    }

    @Test
    public void testGetAllRooms() {
        RoomResponseDTO responseDTO = new RoomResponseDTO();
        List<Room> rooms = new ArrayList<>();

        when(roomRepository.findAll()).thenReturn(rooms);
        when(roomMapper.toDTO(any(Room.class))).thenReturn(responseDTO);

        List<RoomResponseDTO> responseDTOS = roomService.getAllRooms();

        assertNotNull(responseDTOS);
        verify(roomRepository, times(1)).findAll();
    }
}
