package az.hotelservice.service.impl;

import az.hotelservice.entity.Room;
import az.hotelservice.entity.RoomType;
import az.hotelservice.exception.RoomAlreadyBookedException;
import az.hotelservice.exception.RoomNotBookedException;
import az.hotelservice.mapper.RoomMapper;
import az.hotelservice.repository.RoomRepository;
import az.hotelservice.repository.RoomTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReservationServiceImplTest {

    @Mock
    private RoomRepository roomRepository;

    @Mock
    private RoomTypeRepository roomTypeRepository;

    @InjectMocks
    private ReservationServiceImpl reservationService;

    @Test
    public void testCancelReservation() {
        UUID hotelId = UUID.randomUUID();
        Long roomId = 1L;

        Room room = new Room();
        RoomType roomType = new RoomType();
        room.setRoomType(roomType);
        room.setCheckInDate(LocalDate.now().minusDays(1));
        room.setCheckOutDate(LocalDate.now().plusDays(1));

        when(roomRepository.findByIdAndHotelId(roomId, hotelId))
                .thenReturn(Optional.of(room));

        Boolean result = reservationService.cancelReservation(hotelId, roomId);

        assertTrue(result);
        assertNull(room.getCheckInDate());
        assertNull(room.getCheckOutDate());

        verify(roomTypeRepository, times(1)).save(roomType);
        verify(roomRepository, times(1)).save(room);
    }


    @Mock
    private RoomMapper roomMapper;

//    @InjectMocks
//    private ReservationServiceImpl reservationService;

    private UUID hotelId;
    private Long roomId;
    private Room room;
    private RoomType roomType;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;

    @BeforeEach
    void setUp() {
        hotelId = UUID.randomUUID();
        roomId = 1L;
        roomType = new RoomType();
        room = new Room();
        room.setRoomType(roomType);
        checkInDate = LocalDate.now();
        checkOutDate = LocalDate.now().plusDays(3);
    }

//    @Test
//    void reserveRoom_ShouldReserveRoomSuccessfully() {
//        room.setCheckInDate(null);
//        room.setCheckOutDate(null);
//        when(roomRepository.findByIdAndHotelId(roomId, hotelId)).thenReturn(Optional.of(room));
//        when(roomMapper.toDTO(any(Room.class))).thenReturn(new RoomResponseDTO());
//        RoomResponseDTO responseDTO = reservationService.reserveRoom(hotelId, roomId, checkInDate, checkOutDate);
//        assertNotNull(responseDTO);
//        assertEquals(checkInDate, room.getCheckInDate());
//        assertEquals(checkOutDate, room.getCheckOutDate());
//        assertFalse(roomType.getIsActive());
//        verify(roomTypeRepository).save(roomType);
//        verify(roomRepository).save(room);
//    }

    @Test
    void reserveRoom_ShouldThrowRoomAlreadyBookedException() {
        room.setCheckInDate(LocalDate.now());
        room.setCheckOutDate(LocalDate.now().plusDays(1));
        when(roomRepository.findByIdAndHotelId(roomId, hotelId)).thenReturn(Optional.of(room));
        assertThrows(RoomAlreadyBookedException.class, () ->
            reservationService.reserveRoom(hotelId, roomId, checkInDate, checkOutDate));
        verify(roomRepository, never()).save(room);
    }

    @Test
    void cancelReservation_ShouldCancelReservationSuccessfully() {
        room.setCheckInDate(checkInDate);
        room.setCheckOutDate(checkOutDate);
        when(roomRepository.findByIdAndHotelId(roomId, hotelId)).thenReturn(Optional.of(room));
        Boolean result = reservationService.cancelReservation(hotelId, roomId);
        assertTrue(result);
        assertNull(room.getCheckInDate());
        assertNull(room.getCheckOutDate());
        assertTrue(roomType.getIsActive());
        verify(roomTypeRepository).save(roomType);
        verify(roomRepository).save(room);
    }

    @Test
    void cancelReservation_ShouldThrowRoomNotBookedException() {
        room.setCheckInDate(null);
        room.setCheckOutDate(null);
        when(roomRepository.findByIdAndHotelId(roomId, hotelId)).thenReturn(Optional.of(room));
        assertThrows(RoomNotBookedException.class, () ->
            reservationService.cancelReservation(hotelId, roomId));
        verify(roomRepository, never()).save(room);
    }
}
