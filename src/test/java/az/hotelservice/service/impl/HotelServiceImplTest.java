package az.hotelservice.service.impl;

import az.hotelservice.dto.requests.HotelRequestDTO;
import az.hotelservice.dto.responses.HotelResponseDTO;
import az.hotelservice.dto.responses.PageHotelResponse;
import az.hotelservice.entity.Hotel;
import az.hotelservice.exception.RecordNotFoundException;
import az.hotelservice.mapper.HotelMapper;
import az.hotelservice.repository.HotelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class HotelServiceImplTest {

    @InjectMocks
    private HotelServiceImpl hotelService;

    @Mock
    private HotelRepository hotelRepository;

    @Mock
    private HotelMapper hotelMapper;

    private Hotel hotel;
    private HotelRequestDTO hotelRequestDTO;
    private HotelResponseDTO hotelResponseDTO;

    @BeforeEach
    void setUp() {
        hotel = new Hotel();
        hotel.setId(UUID.randomUUID());
        hotel.setName("Test Hotel");

        hotelRequestDTO = new HotelRequestDTO();
        hotelRequestDTO.setName("Test Hotel");

        hotelResponseDTO = new HotelResponseDTO();
        hotelResponseDTO.setName("Test Hotel");
        Mockito.lenient().when(hotelMapper.toEntity(any(HotelRequestDTO.class))).thenReturn(hotel);
        Mockito.lenient().when(hotelMapper.toDTO(any(Hotel.class))).thenReturn(hotelResponseDTO);
        Mockito.lenient().when(hotelRepository.save(any(Hotel.class))).thenReturn(hotel);

    }


    @Test
    void createHotel_ShouldReturnHotelResponseDTO() {
        when(hotelRepository.save(any(Hotel.class))).thenReturn(hotel);
        HotelResponseDTO result = hotelService.createHotel(hotelRequestDTO);

        assertNotNull(result);
        assertEquals(hotelResponseDTO.getName(), result.getName());
        verify(hotelRepository, times(1)).save(hotel);
    }

    @Test
    void updateHotel_ShouldReturnUpdatedHotelResponseDTO() {
        UUID hotelId = hotel.getId();
        when(hotelRepository.findById(hotelId)).thenReturn(Optional.of(hotel));
        when(hotelRepository.save(any(Hotel.class))).thenReturn(hotel); // Mock save method

        HotelResponseDTO result = hotelService.updateHotel(hotelId, hotelRequestDTO);

        assertNotNull(result);
        assertEquals(hotelResponseDTO.getName(), result.getName());
        verify(hotelRepository, times(1)).findById(hotelId);
        verify(hotelRepository, times(1)).save(hotel);
    }

    @Test
    void updateHotel_ShouldThrowRecordNotFoundException() {
        UUID hotelId = hotel.getId();
        when(hotelRepository.findById(hotelId)).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, () -> hotelService.updateHotel(hotelId, hotelRequestDTO));
        verify(hotelRepository, times(1)).findById(hotelId);
        verify(hotelRepository, times(0)).save(any(Hotel.class));
    }

    @Test
    void getHotelById_ShouldReturnHotelResponseDTO() {
        UUID hotelId = hotel.getId();
        when(hotelRepository.findById(hotelId)).thenReturn(Optional.of(hotel));

        HotelResponseDTO result = hotelService.getHotelById(hotelId);

        assertNotNull(result);
        assertEquals(hotelResponseDTO.getName(), result.getName());
        verify(hotelRepository, times(1)).findById(hotelId);
    }

    @Test
    void getHotelById_ShouldThrowRecordNotFoundException() {
        UUID hotelId = hotel.getId();
        when(hotelRepository.findById(hotelId)).thenReturn(Optional.empty());

        assertThrows(RecordNotFoundException.class, () -> hotelService.getHotelById(hotelId));
        verify(hotelRepository, times(1)).findById(hotelId);
    }

    @Test
    void getAllHotels_ShouldReturnPageHotelResponse() {
        Pageable pageable = PageRequest.of(0, 10);
        Page<Hotel> hotelPage = new PageImpl<>(Arrays.asList(hotel), pageable, 1);

        when(hotelRepository.findAll(pageable)).thenReturn(hotelPage);

        PageHotelResponse result = hotelService.getAllHotels(0, 10);

        assertNotNull(result);
        assertEquals(1, result.getTotalElements());
        verify(hotelRepository, times(1)).findAll(pageable);
    }

    @Test
    void getAllHotels_ShouldThrowRecordNotFoundException() {
        Pageable pageable = PageRequest.of(0, 10);
        Page<Hotel> hotelPage = Page.empty(pageable);

        when(hotelRepository.findAll(pageable)).thenReturn(hotelPage);

        assertThrows(RecordNotFoundException.class, () -> hotelService.getAllHotels(0, 10));
        verify(hotelRepository, times(1)).findAll(pageable);
    }

    @Test
    void deleteHotel_ShouldInvokeRepositoryDelete() {
        UUID hotelId = hotel.getId();

        hotelService.deleteHotel(hotelId);

        verify(hotelRepository, times(1)).deleteById(hotelId);
    }

}
