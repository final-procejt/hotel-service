package az.hotelservice.repository;

import az.hotelservice.entity.RoomType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class RoomTypeRepositoryTest {
    @Autowired
    private RoomTypeRepository roomTypeRepository;

    @Test
    public void testSavedRoomType() {
        RoomType roomType = RoomType.builder()
                .roomCapacity(3)
                .rentPerDay(99.99)
                .isActive(true).build();
        RoomType savedRoomType = roomTypeRepository.save(roomType);

        assertNotNull(savedRoomType);
        assertEquals(savedRoomType.getRoomCapacity(), 3);
        assertEquals(savedRoomType.getRentPerDay(), Double.valueOf(99.99));
        assertEquals(savedRoomType.getIsActive(), true);
    }
}
