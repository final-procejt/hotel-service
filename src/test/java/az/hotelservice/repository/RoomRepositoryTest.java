package az.hotelservice.repository;

import az.hotelservice.entity.Hotel;
import az.hotelservice.entity.Room;
import az.hotelservice.entity.RoomType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class RoomRepositoryTest {
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private HotelRepository hotelRepository;


    @Test
    public void testSaveRoom() {
        Hotel hotel = Hotel.builder().name("Hotel")
                .stars(4).description("Cool!").build();

        RoomType roomType = RoomType.builder()
                .rentPerDay(145.25)
                .roomCapacity(2).isActive(true).build();

        Room room = Room.builder()
                .hotel(hotel)
                .roomType(roomType)
                .checkInDate(LocalDate.of(2024, 7, 15))
                .checkOutDate(LocalDate.of(2024, 7, 19))
                .build();

        Room savedRoom = roomRepository.save(room);

        assertNotNull(savedRoom);
        assertEquals(savedRoom.getRoomType(), room.getRoomType());
        assertEquals(savedRoom.getCheckInDate(), room.getCheckInDate());
        assertEquals(savedRoom.getCheckOutDate(), room.getCheckOutDate());
        assertEquals(savedRoom.getHotel(), room.getHotel());
    }

    @Test
    public void findByIdAndHotelIdTest() {
        Hotel hotel = Hotel.builder().name("Hilton Baku")
                .stars(5).description("Amazing!").build();
        Hotel savedHotel = hotelRepository.save(hotel);
        RoomType roomType = RoomType.builder()
                .rentPerDay(145.25)
                .roomCapacity(2).isActive(true).build();
        Room room = Room.builder().roomType(roomType).hotel(hotel)
                .checkOutDate(LocalDate.now().minusDays(1))
                .checkOutDate(LocalDate.now().plusDays(4)).build();
        Room savedRoom = roomRepository.save(room);

        Optional<Room> result = roomRepository.findByIdAndHotelId(savedRoom.getId(), savedHotel.getId());

        assertTrue(result.isPresent());
        assertEquals(savedRoom.getId(), result.get().getId());
        assertEquals(savedRoom.getHotel().getId(), result.get().getHotel().getId());
    }

    @Test
    public void testDeleteRoom() {
        Hotel hotel = Hotel.builder().name("Hilton Baku")
                .stars(5).description("Amazing!").build();
        hotelRepository.save(hotel);
        RoomType roomType = RoomType.builder()
                .rentPerDay(145.25)
                .roomCapacity(2).isActive(true).build();
        Room room = Room.builder().roomType(roomType).hotel(hotel)
                .checkOutDate(LocalDate.now().minusDays(1))
                .checkOutDate(LocalDate.now().plusDays(4)).build();
        Room savedRoom = roomRepository.save(room);
        roomRepository.delete(savedRoom);

        Optional<Room> deletedRoom = roomRepository.findByIdAndHotelId(room.getId(), hotel.getId());

        assertTrue(deletedRoom.isEmpty());
    }

    @Test
    public void testGetAllRooms() {
        Hotel hotel = Hotel.builder()
                .name("Hilton Baku")
                .stars(5)
                .description("Amazing!")
                .build();
        hotelRepository.save(hotel);

        Room room1 = Room.builder()
                .hotel(hotel)
                .checkInDate(LocalDate.now().minusDays(1))
                .checkOutDate(LocalDate.now().plusDays(4))
                .build();
        Room room2 = Room.builder()
                .hotel(hotel)
                .checkInDate(LocalDate.now().minusDays(2))
                .checkOutDate(LocalDate.now().plusDays(5))
                .build();

        roomRepository.save(room1);
        roomRepository.save(room2);

        List<Room> rooms = roomRepository.findAll();

        assertNotNull(rooms);
        assertEquals(rooms.size(), 2);
    }

    @Test
    public void testGetRoomById() {
        Hotel hotel = Hotel.builder()
                .name("Hilton Baku")
                .stars(5)
                .description("Amazing!")
                .build();
        hotelRepository.save(hotel);
        RoomType roomType = RoomType.builder()
                .rentPerDay(145.25)
                .roomCapacity(2).isActive(true).build();

        Room room = Room.builder().roomType(roomType).hotel(hotel)
                .checkOutDate(LocalDate.now().minusDays(1))
                .checkOutDate(LocalDate.now().plusDays(4)).build();

        Room savedRoom = roomRepository.save(room);
        Optional<Room> checkExistenceOfRoom = roomRepository.findById(savedRoom.getId());

        assertTrue(checkExistenceOfRoom.isPresent());
    }

    @Test
    public void testUpdateRoom() {
        Hotel hotel = Hotel.builder().name("Hilton Baku")
                .stars(5).description("Amazing!").build();
        hotelRepository.save(hotel);

        RoomType roomType = RoomType.builder()
                .rentPerDay(145.25)
                .roomCapacity(2).isActive(true).build();

        Room room = Room.builder().roomType(roomType).hotel(hotel)
                .checkOutDate(LocalDate.now().minusDays(1))
                .checkOutDate(LocalDate.now().plusDays(4)).build();
        Room savedRoom = roomRepository.save(room);

        savedRoom.setCheckInDate(LocalDate.now().minusDays(0));
        savedRoom.setCheckOutDate(LocalDate.now().plusDays(2));

        Room updatedRoom = roomRepository.save(savedRoom);

        assertNotNull(updatedRoom);
        assertEquals(LocalDate.now(), updatedRoom.getCheckInDate());
        assertEquals(LocalDate.now().plusDays(2), updatedRoom.getCheckOutDate());

    }

}
