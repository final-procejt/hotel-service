package az.hotelservice.repository;

import az.hotelservice.entity.Address;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class AddressRepositoryTest {
    @Autowired
    private AddressRepository addressRepository;

    @Test
    public void testSavedAddress() {
        Address address = Address.builder()
                .street("123 Main St")
                .state("NY")
                .city("New York")
                .pinCode(1001)
                .state("NY")
                .country("USA")
                .build();

        Address savedAddress = addressRepository.save(address);
        assertNotNull(savedAddress);
        assertEquals(savedAddress.getCity(), "New York");
        assertEquals(savedAddress.getPinCode(), 1001);
    }

    @Test
    public void testGetAllAddress() {
        Address address1 = Address.builder()
                .street("123 Main St").state("NY").city("New York")
                .pinCode(1001).state("NY").country("USA").build();
        Address address2 = Address.builder()
                .street("123 Main St").state("NY").city("New York")
                .pinCode(1001).state("NY").country("USA").build();

        addressRepository.save(address1);
        addressRepository.save(address2);

        List<Address> addresses = addressRepository.findAll();

        assertNotNull(addresses);
        assertEquals(addresses.size(), 2);
    }

    @Test
    public void testUpdateAddress() {
        Address address = Address.builder()
                .street("123 Main St").state("NY").city("New York")
                .pinCode(1001).state("NY").country("USA").build();
        Address savedAddress = addressRepository.save(address);

        savedAddress.setStreet("456 Elm St");
        savedAddress.setCity("Los Angeles");
        savedAddress.setState("CA");
        savedAddress.setPinCode(90001);
        savedAddress.setCountry("USA");

        addressRepository.save(savedAddress);

        List<Address> addresses = addressRepository.findAll();
        Address result = addresses.get(0);

        assertEquals("456 Elm St", result.getStreet());
        assertEquals(90001, result.getPinCode());
        assertEquals("USA", result.getCountry());
        assertEquals("CA", result.getState());
        assertEquals("Los Angeles", result.getCity());
    }

    @Test
    public void testDeleteAddress() {
        Address address = Address.builder()
                .street("123 Main St").state("NY").city("New York")
                .pinCode(1001).state("NY").country("USA").build();

        Address savedAddress = addressRepository.save(address);
        addressRepository.delete(savedAddress);

        List<Address> addresses = addressRepository.findAll();
        assertTrue(addresses.isEmpty());
    }
}
