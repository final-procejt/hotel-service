package az.hotelservice.repository;

import az.hotelservice.entity.Hotel;
import az.hotelservice.entity.Review;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ReviewRepositoryTest {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private HotelRepository hotelRepository;

    @Test
    public void testSaveReview() {
        Hotel hotel = Hotel.builder()
                .name("Hotel 1")
                .stars(3)
                .description("Great")
                .build();
        hotel = hotelRepository.save(hotel);

        Review review = Review.builder()
                .message("Good")
                .rating(3)
                .hotel(hotel)
                .build();

        Review savedReview = reviewRepository.save(review);

        assertNotNull(savedReview);
        assertEquals(savedReview.getMessage(), "Good");
        assertEquals(review.getRating(), savedReview.getRating());
    }

    @Test
    public void testUpdateReview() {
        Hotel hotel = Hotel.builder()
                .name("Hotel 2")
                .stars(4)
                .description("Cool!")
                .build();
        hotel = hotelRepository.save(hotel);

        Review review = Review.builder()
                .message("Not Bad")
                .rating(4)
                .hotel(hotel)
                .userId(1)
                .build();

        Review savedReview = reviewRepository.save(review);

        savedReview.setRating(2);
        savedReview.setMessage("Bad");

        Review updatedReview = reviewRepository.save(savedReview);

        Optional<Review> result = reviewRepository.findById(review.getId());

        assertTrue(result.isPresent());
        assertEquals(2, updatedReview.getRating());
        assertEquals("Bad", updatedReview.getMessage());
    }

    @Test
    public void testDeleteReview() {
        Hotel hotel = Hotel.builder()
                .name("Hotel 2")
                .stars(4)
                .description("Cool!")
                .build();
        hotel = hotelRepository.save(hotel);

        Review review = Review.builder()
                .message("Not Bad")
                .rating(4)
                .hotel(hotel)
                .userId(1)
                .build();

        Review savedReview = reviewRepository.save(review);

        reviewRepository.deleteById(savedReview.getId());

        Optional<Review> result = reviewRepository.findById(savedReview.getId());
        assertFalse(result.isPresent());
    }

    @Test
    public void testFindByIdReview() {
        Hotel hotel = Hotel.builder()
                .name("Hotel 2")
                .stars(5)
                .description("Great!")
                .build();
        hotel = hotelRepository.save(hotel);
        Review review = Review.builder()
                .message("Wow")
                .rating(5)
                .hotel(hotel)
                .userId(1)
                .build();
        Review savedReview = reviewRepository.save(review);

        Review result = reviewRepository.findById(savedReview.getId()).get();

        assertNotNull(result);
    }

    @Test
    public void testGetAllReview() {
        Hotel hotel = Hotel.builder()
                .name("Hotel")
                .stars(4)
                .description("Cool!")
                .build();
        hotel = hotelRepository.save(hotel);

        Review review1 = Review.builder().message("Good")
                .rating(4).hotel(hotel)
                .userId(1).build();
        Review review2 = Review.builder().message("Not Bad")
                .rating(3).hotel(hotel)
                .userId(1).build();
        Review review3 = Review.builder().message("Bad")
                .rating(2).hotel(hotel)
                .userId(1).build();

        reviewRepository.save(review1);
        reviewRepository.save(review2);
        reviewRepository.save(review3);

        List<Review> reviews = reviewRepository.findAll();
        assertNotNull(reviews);
        assertEquals(reviews.size(), 3);
    }
}
