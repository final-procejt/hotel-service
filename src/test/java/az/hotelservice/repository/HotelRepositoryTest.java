package az.hotelservice.repository;

import az.hotelservice.entity.Address;
import az.hotelservice.entity.Hotel;
import az.hotelservice.entity.Room;
import az.hotelservice.entity.RoomType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class HotelRepositoryTest {
    @Autowired
    private HotelRepository hotelRepository;

    @BeforeEach
    public void setUp() {
        Address address = Address.builder()
                .city("New York").pinCode(10001).street("5th Avenue")
                .country("USA").build();
        RoomType roomType = RoomType.builder()
                .rentPerDay(145.25)
                .roomCapacity(2).isActive(true).build();
        Room room = Room.builder().roomType(roomType)
                .checkOutDate(LocalDate.now().minusDays(1))
                .checkOutDate(LocalDate.now().plusDays(4)).build();
        Hotel hotel = Hotel.builder().name("Even Hotel New York")
                .stars(4).address(address).description("New Hotel")
                .rooms(List.of(room)).build();
        room.setHotel(hotel);
        hotelRepository.save(hotel);
    }

    @Test
    public void testFindHotelsByCriteria() {
        List<Hotel> hotels = hotelRepository
                .findHotelsByCriteria("USA", "New York", 2, BigDecimal.valueOf(100.0), BigDecimal.valueOf(300.0));

        assertNotNull(hotels);
        assertFalse(hotels.isEmpty());
        assertEquals("Even Hotel New York", hotels.get(0).getName());
    }

    @Test
    public void testFindAll() {
        PageRequest pageRequest = PageRequest.of(0, 10);
        Page<Hotel> page = hotelRepository.findAll(pageRequest);
        assertNotNull(page);
        assertTrue(page.hasContent());
    }
}
