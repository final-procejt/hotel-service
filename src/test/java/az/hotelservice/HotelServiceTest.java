package az.hotelservice;

import az.hotelservice.dto.AddressDTO;
import az.hotelservice.dto.requests.HotelRequestDTO;
import az.hotelservice.dto.requests.HotelSearchRequest;
import az.hotelservice.dto.responses.HotelResponseDTO;
import az.hotelservice.dto.responses.PageHotelResponse;
import az.hotelservice.entity.Address;
import az.hotelservice.entity.Hotel;
import az.hotelservice.exception.RecordNotFoundException;
import az.hotelservice.mapper.HotelMapper;
import az.hotelservice.repository.HotelRepository;
import az.hotelservice.service.impl.HotelServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HotelServiceTest {

    @Mock
    private HotelRepository hotelRepository;
    @Mock
    private HotelMapper hotelMapper;
    @InjectMocks
    private HotelServiceImpl hotelService;
    private Hotel hotel;
    private HotelRequestDTO hotelRequestDTO;
    private HotelResponseDTO hotelResponseDTO;
    private Address address;


    @BeforeEach
    void setUp() {
        hotel = new Hotel();
        hotel.setId(UUID.randomUUID());

        address = new Address();
        address.setCity("Test City");
        address.setStreet("Test Street");
        address.setState("Test State");
        address.setCountry("Test Country");
        address.setPinCode(10005);
        hotel.setAddress(address);

        hotelRequestDTO = new HotelRequestDTO();
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setCity("Test City");
        addressDTO.setStreet("Test Street");
        addressDTO.setState("Test State");
        addressDTO.setCountry("Test Country");
        addressDTO.setPinCode(10005);
        hotelRequestDTO.setAddress(addressDTO);

        hotelResponseDTO = new HotelResponseDTO();
        hotelResponseDTO.setId(hotel.getId());
    }

    @Test
    void createHotel_Success() {
        when(hotelMapper.toEntity(any(HotelRequestDTO.class))).thenReturn(hotel);
        when(hotelRepository.save(any(Hotel.class))).thenReturn(hotel);
        when(hotelMapper.toDTO(any(Hotel.class))).thenReturn(hotelResponseDTO);
        HotelResponseDTO createdHotel = hotelService.createHotel(hotelRequestDTO);
        assertNotNull(createdHotel);
        verify(hotelRepository, times(1)).save(any(Hotel.class));
        verify(hotelMapper, times(1)).toDTO(any(Hotel.class));
    }

    @Test
    void updateHotel_Success() {
        UUID hotelId = hotel.getId();
        when(hotelRepository.findById(any(UUID.class))).thenReturn(Optional.of(hotel));
        when(hotelRepository.save(any(Hotel.class))).thenReturn(hotel);
        when(hotelMapper.toDTO(any(Hotel.class))).thenReturn(hotelResponseDTO);
        HotelResponseDTO updatedHotel = hotelService.updateHotel(hotelId, hotelRequestDTO);
        assertNotNull(updatedHotel);
        assertEquals(hotelId, updatedHotel.getId());
        verify(hotelRepository, times(1)).findById(hotelId);
        verify(hotelRepository, times(1)).save(any(Hotel.class));
        verify(hotelMapper, times(1)).toDTO(any(Hotel.class));
    }

    @Test
    void updateHotel_NotFound() {
        UUID hotelId = UUID.randomUUID();
        when(hotelRepository.findById(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(RecordNotFoundException.class, () -> {
            hotelService.updateHotel(hotelId, hotelRequestDTO);
        });
        verify(hotelRepository, times(1)).findById(hotelId);
    }

    @Test
    void getHotelById_Success() {
        UUID hotelId = UUID.randomUUID();
        when(hotelRepository.findById(any(UUID.class))).thenReturn(Optional.of(hotel));
        when(hotelMapper.toDTO(any(Hotel.class))).thenReturn(hotelResponseDTO);
        HotelResponseDTO foundHotel = hotelService.getHotelById(hotelId);
        assertNotNull(foundHotel);
        verify(hotelRepository, times(1)).findById(hotelId);
        verify(hotelMapper, times(1)).toDTO(any(Hotel.class));
    }

    @Test
    void getHotelById_NotFound() {
        UUID hotelId = UUID.randomUUID();
        when(hotelRepository.findById(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(RecordNotFoundException.class, () -> {
            hotelService.getHotelById(hotelId);
        });
        verify(hotelRepository, times(1)).findById(hotelId);
    }

    @Test
    void deleteHotel_Success() {
        UUID hotelId = UUID.randomUUID();
        doNothing().when(hotelRepository).deleteById(any(UUID.class));
        hotelService.deleteHotel(hotelId);
        verify(hotelRepository, times(1)).deleteById(hotelId);
    }

    @Test
    void searchHotelCriteria_Success() {
        HotelSearchRequest searchRequest = new HotelSearchRequest();
        int page = 0;
        int size = 10;
        List<Hotel> hotels = new ArrayList<>();
        hotels.add(hotel);
        Page<Hotel> hotelPage = new PageImpl<>(hotels, PageRequest.of(page, size), hotels.size());
        when(hotelRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(hotelPage);
        when(hotelMapper.toDTO(any(Hotel.class))).thenReturn(hotelResponseDTO);
        PageHotelResponse response = hotelService.searchHotelCriteria(searchRequest, page, size);
        assertNotNull(response);
        assertEquals(1, response.getTotalElements());
        assertEquals(1, response.getTotalPages());
        assertEquals(1, response.getHotelResponse().size());
        assertEquals(hotelResponseDTO, response.getHotelResponse().get(0));
        verify(hotelRepository, times(1)).findAll(any(Specification.class), any(Pageable.class));
        verify(hotelMapper, times(1)).toDTO(any(Hotel.class));
    }

    @Test
    void searchHotelCriteria_RecordNotFoundException() {
        HotelSearchRequest searchRequest = new HotelSearchRequest();
        int page = 0;
        int size = 10;
        Page<Hotel> hotelPage = new PageImpl<>(new ArrayList<>(), PageRequest.of(page, size), 0);
        when(hotelRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(hotelPage);
        assertThrows(RecordNotFoundException.class, () -> {
            hotelService.searchHotelCriteria(searchRequest, page, size);
        });
        verify(hotelRepository, times(1)).findAll(any(Specification.class), any(Pageable.class));
        verify(hotelMapper, times(0)).toDTO(any(Hotel.class));
    }
}
